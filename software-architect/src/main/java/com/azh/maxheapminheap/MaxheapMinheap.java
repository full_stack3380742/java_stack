package com.azh.maxheapminheap;

import com.alibaba.fastjson.JSON;
import com.azh.Entity.DataItemEntity;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.PriorityBlockingQueue;

public class MaxheapMinheap {
    // Java示例：使用最小堆来维护前5名数据
    private static PriorityBlockingQueue<DataItemEntity> top5Queue = new PriorityBlockingQueue<>(5, Comparator.comparingDouble(DataItemEntity::getResolutionRate));
    static PriorityBlockingQueue<DataItemEntity> bottom2Queue = new PriorityBlockingQueue<>(2, (a, b) -> Double.compare(b.getResolutionRate(), a.getResolutionRate()));
    public static void main(String[] args) {
        DataItemEntity[] dataItems = {
                new DataItemEntity(1, 0.95),
                new DataItemEntity(2, 0.85),
                new DataItemEntity(3, 0.92),
                new DataItemEntity(4, 0.89),
                new DataItemEntity(5, 0.88),
/*                new DataItemEntity(6, 0.99),
                new DataItemEntity(7, 0.95)*/
        };
        // 遍历分页查询结果
        for (DataItemEntity item : dataItems) {
            if (top5Queue.size() < 5) {
                top5Queue.add(item);
            } else if (item.getResolutionRate() > top5Queue.peek().getResolutionRate()) {
                top5Queue.poll();
                top5Queue.add(item);
            }

            if (bottom2Queue.size() < 2) {
                bottom2Queue.add(item);
            } else if (item.getResolutionRate() < bottom2Queue.peek().getResolutionRate()) {
                bottom2Queue.poll();
                bottom2Queue.add(item);
            }
        }

        while (!top5Queue.isEmpty()) {
            System.out.println(JSON.toJSONString(top5Queue.poll()));
        }
        System.out.println();
        while (!bottom2Queue.isEmpty()) {
            System.out.println(JSON.toJSONString(bottom2Queue.poll()));
        }
    }
}
