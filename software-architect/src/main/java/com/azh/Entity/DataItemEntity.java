package com.azh.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DataItemEntity {
    private int id;
    private double resolutionRate;


}
