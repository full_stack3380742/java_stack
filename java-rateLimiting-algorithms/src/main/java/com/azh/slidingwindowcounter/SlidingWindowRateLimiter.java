package com.azh.slidingwindowcounter;

import java.util.Deque;
import java.util.LinkedList;

public class SlidingWindowRateLimiter {
    private final long windowSizeInMillis; // 窗口大小，单位毫秒
    private final int maxRequests;         // 窗口内允许的最大请求数
    private final Deque<Long> requests = new LinkedList<>(); // 存储请求时间的队列

    public SlidingWindowRateLimiter(long windowSizeInMillis, int maxRequests) {
        this.windowSizeInMillis = windowSizeInMillis;
        this.maxRequests = maxRequests;
    }

    public synchronized boolean tryAcquire() {
        long now = System.currentTimeMillis();
        // 清理过期的请求记录
        while (!requests.isEmpty() && now - requests.peekFirst() >= windowSizeInMillis) {
            requests.pollFirst();
        }
        // 检查当前窗口内的请求量是否超过限制
        if (requests.size() < maxRequests) {
            requests.addLast(now);
            return true; // 请求成功
        } else {
            return false; // 请求被拒绝
        }
    }

    public static void main(String[] args) throws InterruptedException {
        // 示例：1秒窗口，最多允许3个请求
        SlidingWindowRateLimiter rateLimiter = new SlidingWindowRateLimiter(1000, 3);

        // 模拟请求
        for (int i = 0; i < 5; i++) {
            System.out.println("Request " + (i + 1) + ": " + rateLimiter.tryAcquire());
            Thread.sleep(200); // 模拟请求间隔时间
        }

        // 模拟稍后的请求以观察窗口滑动效果
        Thread.sleep(800);
        for (int i = 0; i < 3; i++) {
            System.out.println("Request " + (i + 6) + ": " + rateLimiter.tryAcquire());
            Thread.sleep(200); // 模拟请求间隔时间
        }
    }
}