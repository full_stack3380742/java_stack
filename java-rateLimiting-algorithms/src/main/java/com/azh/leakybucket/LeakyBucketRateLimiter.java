package com.azh.leakybucket;

import java.util.concurrent.TimeUnit;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class LeakyBucketRateLimiter {
    private final int bucketCapacity; // 漏桶容量
    private final int consumeRate; // 请求处理速率（每秒处理请求数）
    private final BlockingQueue<Runnable> bucket; // 请求队列

    public LeakyBucketRateLimiter(int bucketCapacity, int consumeRate) {
        this.bucketCapacity = bucketCapacity;
        this.consumeRate = consumeRate;
        this.bucket = new LinkedBlockingQueue<>(bucketCapacity);
        startConsuming(); // 启动定期消费请求的线程
    }

    // 添加请求到漏桶
    public boolean addRequest(Runnable request) {
        if (bucket.offer(request)) {
            System.out.println("请求已添加到漏桶");
            return true;
        } else {
            System.out.println("请求被丢弃：漏桶已满");
            return false;
        }
    }

    // 启动请求的定期消费任务
    private void startConsuming() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(() -> {
            for (int i = 0; i < consumeRate; i++) {
                Runnable request = bucket.poll();
                if (request != null) {
                    request.run();
                }
            }
        }, 0, 1, TimeUnit.SECONDS); // 每秒消费一次
    }

    public static void main(String[] args) {
        LeakyBucketRateLimiter rateLimiter = new LeakyBucketRateLimiter(5, 2);

        // 模拟10个请求
        for (int i = 0; i < 10; i++) {
            rateLimiter.addRequest(() -> System.out.println("处理请求：" + System.currentTimeMillis()));
            try {
                Thread.sleep(200); // 模拟请求间隔
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}