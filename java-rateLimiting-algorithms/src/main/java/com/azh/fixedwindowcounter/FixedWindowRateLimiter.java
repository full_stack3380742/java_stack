package com.azh.fixedwindowcounter;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 固定窗口计数器限流算法
 */
public class FixedWindowRateLimiter {
    private final int maxRequests;  // 每个时间窗口允许的最大请求数
    private final long windowSizeInMillis;  // 时间窗口大小（毫秒）
    private AtomicInteger currentRequests = new AtomicInteger(0);  // 当前窗口内的请求数
    private ScheduledExecutorService scheduler;

    public FixedWindowRateLimiter(int maxRequests, long windowSizeInSeconds) {
        this.maxRequests = maxRequests;
        this.windowSizeInMillis = windowSizeInSeconds * 1000;
        this.scheduler = Executors.newScheduledThreadPool(1);
        resetCounter();
    }

    // 尝试获取许可
    public boolean tryAcquire() {
        if (currentRequests.get() < maxRequests) {
            currentRequests.incrementAndGet();
            return true;
        }
        return false;
    }

    // 定期重置计数器
    private void resetCounter() {
        currentRequests.set(0);
        scheduler.schedule(this::resetCounter, windowSizeInMillis, TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) throws InterruptedException {
        int maxRequests = 10;  // 每秒最大请求数
        long windowSizeInSeconds = 1;  // 时间窗口大小为1秒
        FixedWindowRateLimiter limiter = new FixedWindowRateLimiter(maxRequests, windowSizeInSeconds);

        for (int i = 0; i < 200; i++) {  // 模拟20次请求
            if (limiter.tryAcquire()) {
                System.out.println("Request " + i + " is allowed.");
            } else {
                System.out.println("Request " + i + " is denied. Try again later.");
            }
            Thread.sleep(10);  // 模拟请求间隔
        }

        // 关闭调度器
        limiter.scheduler.shutdown();
    }
}
