package com.azh.fixedwindowcounter;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*
 * 固定时间窗口的速率限制器-突显临界问题
 */
public class FixedTimeWindowRateLimiter {
    private final int maxRequests;  // 每个时间窗口允许的最大请求数
    private final long windowSizeInMillis;  // 时间窗口大小（毫秒）
    private AtomicInteger currentRequests = new AtomicInteger(0);  // 当前窗口内的请求数
    private ScheduledExecutorService scheduler;

    public FixedTimeWindowRateLimiter(int maxRequests, long windowSizeInSeconds) {
        this.maxRequests = maxRequests;
        this.windowSizeInMillis = windowSizeInSeconds * 1000;
        this.scheduler = Executors.newScheduledThreadPool(1);
        resetCounter();
    }

    // 尝试获取许可
    public boolean tryAcquire() {
        if (currentRequests.get() < maxRequests) {
            currentRequests.incrementAndGet();
            return true;
        }
        return false;
    }

    // 定期重置计数器
    private void resetCounter() {
        currentRequests.set(0);
        System.out.println("Resetting counter at: " + System.currentTimeMillis());
        scheduler.schedule(this::resetCounter, windowSizeInMillis, TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) throws InterruptedException {
        int maxRequests = 5;  // 每秒最大请求数
        long windowSizeInSeconds = 1;  // 时间窗口大小为1秒
        FixedTimeWindowRateLimiter limiter = new FixedTimeWindowRateLimiter(maxRequests, windowSizeInSeconds);

        for (int i = 0; i < 200; i++) {  // 模拟20次请求
            if (limiter.tryAcquire()) {
                System.out.println("Request " + i + " is allowed at: " + System.currentTimeMillis());
            } else {
                System.out.println("Request " + i + " is denied at: " + System.currentTimeMillis());
            }
            Thread.sleep(200);  // 请求间隔设置得稍微长一点以观察效果
        }

        // 关闭调度器
        limiter.scheduler.shutdown();
    }
}