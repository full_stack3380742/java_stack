package com.azh.tokenbucket;

import java.util.concurrent.atomic.AtomicLong;

/**
 * @author azh
 * @description: 令牌桶算法简单实现
 */
public class TokenBucketLimiter {
    private final int rate;  // 令牌添加速率（每秒添加的令牌数）
    private final int capacity;  // 桶的容量
    private final AtomicLong tokens = new AtomicLong(0);  // 当前桶中的令牌数
    private final AtomicLong lastRefillTime = new AtomicLong(System.nanoTime());  // 上次填充令牌的时间

    public TokenBucketLimiter(int rate, int capacity) {
        this.rate = rate;
        this.capacity = capacity;
    }

    /**
     * 尝试消费指定数量的令牌。
     * @param tokens 要消耗的令牌数量
     * @return 如果成功消费返回true，否则返回false
     */
    public boolean tryConsume(long tokens) {
        refill();
        return this.tokens.getAndUpdate(current -> Math.max(0, current - tokens)) >= tokens;
    }

    /**
     * 根据时间间隔向桶中添加令牌。
     */
    private void refill() {
        long now = System.nanoTime();
        long timeSinceLastRefill = now - lastRefillTime.get();
        long tokensToAdd = (timeSinceLastRefill * rate) / 1_000_000_000L;  // 计算应该添加多少令牌
        if (tokensToAdd > 0) {
            this.tokens.updateAndGet(current -> Math.min(capacity, current + tokensToAdd));
            lastRefillTime.set(now);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        TokenBucketLimiter bucket = new TokenBucketLimiter(1, 10);  // 每秒添加1个令牌，桶容量为10
        for (int i = 0; i < 15; i++) {
            if (bucket.tryConsume(1)) {
                System.out.println("Request " + i + " processed.");
            } else {
                System.out.println("Request " + i + " rejected.");
            }
            Thread.sleep(200);  // 模拟请求间隔
        }
    }
}
