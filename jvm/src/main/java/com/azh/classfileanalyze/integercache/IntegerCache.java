package com.azh.classfileanalyze.integercache;

/**
 * 通过观察字节码来定位 Integer 的缓存机制
 */
public class IntegerCache {
    public static void main(String[] args) {
        Integer int1 = 10;
        Integer int2 = 10;
        // true
        System.out.println(int1 == int2);

        Integer int3 = 128;
        Integer int4 = 128;
        // false
        System.out.println(int3 == int4);
    }
}
