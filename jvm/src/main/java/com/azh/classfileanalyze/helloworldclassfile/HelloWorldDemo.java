package com.azh.classfileanalyze.helloworldclassfile;

import cn.hutool.core.date.DateUtil;

/**
 * 用来观察字节码
 */
public class HelloWorldDemo {
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
