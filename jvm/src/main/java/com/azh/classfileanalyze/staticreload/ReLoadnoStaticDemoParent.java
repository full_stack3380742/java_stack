package com.azh.classfileanalyze.staticreload;

/**
 * 通过观察字节码来证实为什么静态类方法不能重载
 */
public class ReLoadnoStaticDemoParent {
    public void reloadMethod() {
        System.out.println("非静态类方法调用");
    }

    public static void staticMethod() {
        System.out.println("父类静态类方法");
    }
}
