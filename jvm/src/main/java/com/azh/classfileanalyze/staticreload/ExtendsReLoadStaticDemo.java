package com.azh.classfileanalyze.staticreload;

public class ExtendsReLoadStaticDemo extends ReLoadnoStaticDemoParent{
    @Override
    public void reloadMethod() {
        System.out.println("重载父类方法，子类重新实现");
    }

    public static void main(String[] args) {
        ExtendsReLoadStaticDemo extendsReLoadStaticDemo = new ExtendsReLoadStaticDemo();
        extendsReLoadStaticDemo.reloadMethod();
        staticMethod();
    }
}
