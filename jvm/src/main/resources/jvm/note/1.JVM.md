# Java-JVM 执行流程总图

以 JDK1.8 位例（尽管JDK到目前为止已经升级到21了）。

JVM 之所以可以跨平台运行，是因为只要编译器生成的文件符合 JVM 定义的规范，就可以被 JVM 识别。不同平台会有不同的 JVM 实现。下述是 HotSpot 虚拟机执行编译文件的过程：

> 1、javac 编译 java 源文件，生成符合 JVM 规范的 .class 文件
>
> 2、JVM 将 .class 文件加载到内存中，通过类加载器 ClassLoader 生成对应的类数据结构。然后将类存放到**方法区中（元数据区）**。
>
> 3、**执行引擎**将类中的代码指令转译为 CPU 可以识别的二进制指令：
>
> > 将类文件中创建的对象存放到**堆**中。
> >
> > 给每个线程分配内存空间（这块内存空间归线程私有）：
> >
> > > **程序计数器：**存储的是正在执行的字节码指令的行号。具体由两部分功能组成：
> > >
> > > 1、如果线程正在执行一个Java方法，这个计数器记录的是正在执行的虚拟机字节码指令的地址。
> > >
> > > 2、如果线程正在执行的是Native方法，那这个计数器的值就为空。
> >
> > > **虚拟机栈：**每个方法执行时都会创建一个栈帧，每个栈帧包含局部变量表、操作数栈、动态连接、方法出口等信息。栈帧中保管了该方法的参数、**局部变量**以及方法执行过程中的**中间结果**。
> >
> > > **本地方法栈：**为虚拟机使用的本地方法服务。它和Java虚拟机栈的作用非常相似，主要的区别在于Java虚拟机栈是为执行Java方法（即字节码）服务，而本地方法栈则是为执行本地方法服务。所谓的本地方法，就是用其他语言（通常是C或C++）实现的方法。本地方法栈可以帮助Java虚拟机调用这些本地方法，从而使Java能够与其他语言融合。初衷是为了融合C/C++语言，使得可以使用不同的编程语言为Java所用。		

流程总图如下所示：

![](../photo/Java-JVM总图.drawio.png)

## Class 文件结构

使用 javap 命令查看到的是一个16进制的文本内容，不易阅读。可以使用 IDEA 的插件查看 CLASS 文件结构。比如入门级的 HelloWorld 的代码结构：
![](../photo/HelloWorld-Class文件结构.png)

从这个图上可以了解 Class 文件的一个大致结构。

学习分析 Class 文件可以帮助我们更好的分析代码，以及一些代码底层的原理。比如下面的一段测试代码：

```java
public class IntegerCache {
    public static void main(String[] args) {
        Integer int1 = 10;
        Integer int2 = 10;
        // true
        System.out.println(int1 == int2);

        Integer int3 = 128;
        Integer int4 = 128;
        // false
        System.out.println(int3 == int4);
    }
}
```

![](../photo/IntegerCacheClass.png)

对 ClassFile 文件分析，在对int1、2、3、4 赋值的时候调用了 Integer.value 的方法，那就可以直接定位到 Integer.value 的方法：

```java
public static Integer valueOf(int i) {
  if (i >= IntegerCache.low && i <= IntegerCache.high)
    return IntegerCache.cache[i + (-IntegerCache.low)];
  return new Integer(i);
}
```

当值大等于 `low = -128` 且小等于 heigh = 127，则从缓存中获取值。否则创建一个新的对象。所以`int3==int4`结果为`false`。

## 分析一道面试题：

> Java 当中的静态⽅法可以重载吗？
>
> 不可以。在 JVM 中静态类方法调用和非静态类方法使用的字节指令不同。非静态类方法使用的是 invokcvirtual，静态类方法使用的是 invokestatic。JVM 调用重载后的方法使用的字节指令是 invokcvirtual，所以静态类方法无法重载。

代码示例：

```java
/**
 * 通过观察字节码来证实为什么静态类方法不能重载
 */
public class ReLoadnoStaticDemoParent {
    public void reloadMethod() {
        System.out.println("非静态类方法调用");
    }

    public static void staticMethod() {
        System.out.println("父类静态类方法");
    }
}

public class ExtendsReLoadStaticDemo extends ReLoadnoStaticDemoParent{
    @Override
    public void reloadMethod() {
        System.out.println("重载父类方法，子类重新实现");
    }

    public static void main(String[] args) {
        ExtendsReLoadStaticDemo extendsReLoadStaticDemo = new ExtendsReLoadStaticDemo();
        extendsReLoadStaticDemo.reloadMethod();
        staticMethod();
    }
}
```

ExtendsReLoadStaticDemo 的字节码指令如下：

```txt
 0 new #6 <com/azh/staticreload/ExtendsReLoadStaticDemo>
 3 dup
 4 invokespecial #7 <com/azh/staticreload/ExtendsReLoadStaticDemo.<init> : ()V>
 7 astore_1
 8 aload_1
 9 invokevirtual #8 <com/azh/staticreload/ExtendsReLoadStaticDemo.reloadMethod : ()V>
12 invokestatic #9 <com/azh/staticreload/ExtendsReLoadStaticDemo.staticMethod : ()V>
15 aload_1
16 invokevirtual #10 <com/azh/staticreload/ExtendsReLoadStaticDemo.staticMethodChild : ()V>
19 return
```

可以很明显地看到静态类方法调用和非静态类方法使用的字节指令。

还可以通过 CLASS 文件来来分析 `try{}catch{}finaly{}`的执行型过程，以及出栈入栈的详细过程。后续详细展开。

# 类加载机制

根据 JVM 执行流程总图，JVM 会根据 CLASS 文件信息对类行进加载。下面就详细地分析一下类的加载过程。

> 类加载机制总结：
>
> 1、类缓存：每个类加载器对他加载过的类都有一个缓存。
>
> 2、双亲委派：向上委托查找、向下委托加载。
>
> 3、沙箱保护机制：不允许应用程序加载 JDK 内部的系统类。





