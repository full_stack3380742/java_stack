import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadLocalTest {
    public static void main(String[] args) {
        ArrayBlockingQueue<Runnable> q = new ArrayBlockingQueue<>(1);
        AtomicInteger flag = new AtomicInteger(0);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                0, 100, 1,
                TimeUnit.SECONDS, q, new ThreadPoolExecutor.DiscardPolicy());

        executor.execute(new Runnable(){
            public void run() {
                System.out.println("任务1在" + Thread.currentThread().getName() + "执行中");
                System.out.println("任务1执行时，队列大小" + q.size());
                loop();
            }
        });

        executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("任务3在"  + Thread.currentThread().getName() + "执行中");
                System.out.println("任务3执行时，队列大小" + q.size());
                loop();
            }
        });

        executor.execute(new Runnable() {
            @Override
            public void run() {
                System.out.println("任务2在"  + Thread.currentThread().getName() + "执行中");
                System.out.println("任务2执行时，队列大小" + q.size());
            }
        });
    }

    public static void loop() {
        while (true){}
    }
}
