package com.azh.threadstatus;

public class ThreadStatusDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(() -> {
            System.out.println("Thread A: starting...");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread A: finished.");
        });

        Thread t2 = new Thread(() -> {
            synchronized (t1) { // 持有 t1 的锁
                System.out.println("Thread B: holding lock on Thread A...");
                try {
                    Thread.sleep(2000);
                    System.out.println("Thread B: checking if Thread A is alive: " + t1.isAlive());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        Thread.sleep(100); // 确保 t1 已启动
        t2.start();

        t1.join(); // 等待 t1 完成
        System.out.println("Main: Thread A isAlive after join: " + t1.isAlive());
    }
}
