package com.azh.future.controller;

import com.azh.common.NamedThreadFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@RestController
public class FutureDemoController {

    private final static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(),
            Runtime.getRuntime().availableProcessors() * 2 - 1, 0, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(500),
            new NamedThreadFactory("getTimeOutTest"));

    @PostMapping("/future/futureGet")
    public int futureDemoGet() {
        // 省侧客群标签信息查询 异步查询
        Future<Integer> future1 = threadPoolExecutor.submit(new Callable<Integer>() {
            @Override
            public Integer call() {
                // 异步操作
                return 4;
            }
        });

        int j = 0;
        // 中心客群标签查询 异步
        Future<Integer> future2 = threadPoolExecutor.submit(new Callable<Integer>() {
            @Override public Integer call() {
                // 异步操作
                return 5;
            }
        });

        int k = 0;
        // 客群标签静态资源配置查询 异步
        Future<Integer> future3 = threadPoolExecutor.submit(new Callable<Integer>() {
            @Override
            public Integer call() {
                // 异步操作
                return 6;
            }
        });


        Integer futureInt1 = 0;
        try {
            futureInt1 = future1.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Integer futureInt2 = 0;
        try {
            futureInt2 = future2.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Integer futureInt3 = 0;
        try {
            futureInt3 = future3.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return futureInt1 + futureInt2 + futureInt3;
    }

    public static void main(String[] args) {
        FutureDemoController futureDemoController = new FutureDemoController();
        int sumCount = futureDemoController.futureDemoGet();
        System.out.println(sumCount);
        threadPoolExecutor.shutdown();
    }
}
