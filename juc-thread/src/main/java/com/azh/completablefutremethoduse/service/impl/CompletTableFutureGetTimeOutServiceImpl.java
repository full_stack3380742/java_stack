package com.azh.completablefutremethoduse.service.impl;

import com.azh.completablefutremethoduse.service.CompletTableFutureGetTimeOutService;
import org.springframework.stereotype.Service;

@Service
public class CompletTableFutureGetTimeOutServiceImpl implements CompletTableFutureGetTimeOutService {
    @Override
    public String complettableFutureGetTimeOut() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // doSomeThing
        }
        return "42";
    }
}
