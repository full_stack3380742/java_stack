package com.azh.completablefutremethoduse.controller;

import com.azh.common.NamedThreadFactory;
import com.azh.completablefutremethoduse.service.CompletTableFutureGetTimeOutService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 测试 CompletableFuture get 超时处理
 */
@RestController
public class CompletableFutureGetTimeoutController {
    private final static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(),
            Runtime.getRuntime().availableProcessors() * 2 - 1, 0, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(50),
            new NamedThreadFactory("getTimeOutTest"));

    @Resource
    private CompletTableFutureGetTimeOutService completTableFutureGetTimeOutService;


    @PostMapping("/futuregettimeout")
    public String futureGetTimeOut(@RequestBody String str) {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(()->completTableFutureGetTimeOutService.complettableFutureGetTimeOut());
        try {
            future.get(1, TimeUnit.MICROSECONDS);
        }catch (Exception e) {
            System.out.println("异常信息：" + e);
            return e.getMessage();
        }
        return "42";
    }


    public static void main(String[] args) {
        CompletableFuture<String> future = CompletableFuture.supplyAsync(()->complettableFutureGetTimeOut());
        try {
            future.get(1, TimeUnit.MICROSECONDS);
        }catch (Exception e) {
            System.out.println("异常信息：" + e);
        }
    }

    private static String complettableFutureGetTimeOut(){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "1";
    }
}
