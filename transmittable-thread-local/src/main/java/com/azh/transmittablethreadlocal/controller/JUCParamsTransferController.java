package com.azh.transmittablethreadlocal.controller;


import com.alibaba.fastjson.JSON;
import com.azh.common.NamedThreadFactory;
import com.azh.transmittablethreadlocal.VO.JUCParamsEntityVO;
import com.azh.transmittablethreadlocal.service.JUCParamsTransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/juc")
@Slf4j
public class JUCParamsTransferController {
    private final static ThreadPoolExecutor executor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(),
            Runtime.getRuntime().availableProcessors() * 2 - 1, 0, TimeUnit.SECONDS,
            new LinkedBlockingDeque<>(50),
            new NamedThreadFactory("JUCParamsTransferController"));

    @Resource
    private JUCParamsTransferService juCParamsTransferService;

    @PostMapping("/getJUCParams")
    public JUCParamsEntityVO getJUCParams(@RequestBody JUCParamsEntityVO params) throws ExecutionException, InterruptedException {
        JUCParamsEntityVO params1 = new JUCParamsEntityVO();
        params1.setAddress(Thread.currentThread().getName());
        params1.setName(Thread.currentThread().getName());
        params1.setPhone(Thread.currentThread().getName());
        CompletableFuture<JUCParamsEntityVO> future = CompletableFuture.supplyAsync(() -> {
            return juCParamsTransferService.setJUCParams(params1);
        }, executor);
        JUCParamsEntityVO jucParamsEntityVO = future.get();
        CompletableFuture<JUCParamsEntityVO> future2 = CompletableFuture.supplyAsync(() ->
                juCParamsTransferService.setJUCParams(params), executor);
        log.info("future1.get->{}", JSON.toJSONString(jucParamsEntityVO));
        JUCParamsEntityVO jucParamsEntityVO1 = future2.get();
        log.info("future2.get->{}", JSON.toJSONString(jucParamsEntityVO1));
        return params;
    }
}
