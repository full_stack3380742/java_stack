package com.azh.transmittablethreadlocal.service.impl;

import com.azh.transmittablethreadlocal.VO.JUCParamsEntityVO;
import com.azh.transmittablethreadlocal.service.JUCParamsTransferService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@Slf4j
public class JUCParamsTransferServiceImpl implements JUCParamsTransferService {

    @Override
    public JUCParamsEntityVO setJUCParams(JUCParamsEntityVO params) {
        JUCParamsEntityVO entity = new JUCParamsEntityVO();
        entity.setName(params.getName());
        entity.setAddress(params.getAddress());
        entity.setPhone(params.getPhone());
        return entity;
    }
}
