package com.azh.transmittablethreadlocal.service;

import com.azh.transmittablethreadlocal.VO.JUCParamsEntityVO;

import javax.servlet.http.HttpServletRequest;

public interface JUCParamsTransferService {
    JUCParamsEntityVO setJUCParams(JUCParamsEntityVO params);
}
