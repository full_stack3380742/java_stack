package com.azh.transmittablethreadlocal.VO;

import lombok.Data;

@Data
public class JUCParamsEntityVO {
    String name;

    String address;

    String phone;
}
