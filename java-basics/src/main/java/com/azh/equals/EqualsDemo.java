package com.azh.equals;

import java.util.HashMap;
import java.util.Hashtable;

/**
 * equals 和 ==  的区别
 *
 * 在 Java 中 == 主要对比的是栈中的内容，分为两种类型：
 *      1、基本数据类型：变量值；
 *      2、引用类型的值：堆中内存对象的地址
 * equals：object 中也和 == 一样，但是一般子类都会对父类 object 的 equals 方法重写。
 */

public class EqualsDemo {
    public static void main(String[] args) {
        String str1 = "hello";
        String str2 = "hello";
        // str3 存储的是堆中的地址
        String str3 = new String("hello");
        // str1 和 str2 都指向同一个地址
        System.out.println(str1 == str2);
        System.out.println(str1 == str3);
        int a = 10;
        int b = 10;
        System.out.println(a == b);
    }
}
