package com.anzhi.concurrenthashmap;

import com.alibaba.fastjson.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapDemo {
    public static void main(String[] args) {
        ConcurrentHashMap<String,Object> concurrentHashMap = new ConcurrentHashMap<>();
        concurrentHashMap.get("test");
        JSONObject jsonObject = new JSONObject();
        LinkedList<Map<String, Object>> objects = new LinkedList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("123", "123");
        objects.add(map);
        jsonObject.put("test", objects);
    }
}
