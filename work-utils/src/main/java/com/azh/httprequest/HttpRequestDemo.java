package com.azh.httprequest;


import cn.hutool.http.HttpRequest;

import java.util.HashMap;
import java.util.Map;

public class HttpRequestDemo {
    public static void main(String[] args) {
        String url = "https://120.193.170.43:10442/artemis/api/cas/v1/tgt/login";
        String appKey = "25510903";
        String appSecret = "3ahZRfJvw0HFUjJPmjkY";


        //链式构建请求
        Map<String, Object> body = new HashMap<>();
        body.put("userCode", "admin");
        body.put("service", "service");
        body.put("language", "zh_CN");
        String result2 = HttpRequest.post(url)
                .header("userId", "admin")//头信息，多个头信息多次调用此方法即可
                .header("domainId", "https://120.193.170.43/")
                .form(body)//表单内容
                .timeout(20000)//超时，毫秒
                .execute().body();
        System.out.println(result2);
    }
}
