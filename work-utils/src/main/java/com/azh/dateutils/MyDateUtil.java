package com.azh.dateutils;

import cn.hutool.core.date.DateUtil;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.Calendar;
import java.util.Date;

/**
 * 日志�?
 */
public final class MyDateUtil {

    /**
     * 日期格式
     **/
    public interface DATE_PATTERN {
        String YYYYMM = "yyyyMM";
        String HHMMSS = "HHmmss";
        String HH_MM_SS = "HH:mm:ss";
        String YYYYMMDD = "yyyyMMdd";
        String YYYY_MM_DD = "yyyy-MM-dd";
        String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
        String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
        String YYYY_UNDER_MONTH = "yyyy_MM";
    }

    public static String formatDate(String dateString, String oldFormat, String newForamt) {
        if (dateString == null || oldFormat == null || newForamt == null) {
            return null;
        }
        try {
            return date2String(string2Date(dateString, oldFormat), newForamt);
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 将Date类型转换成String类型
     *
     * @param date Date对象
     * @return 形如:"yyyy-MM-dd HH:mm:ss"
     */
    public static String date2String(Date date) {
        return date2String(date, DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
    }

    /**
     * 将Date按格式转化成String
     *
     * @param date    Date对象
     * @param pattern 日期类型
     * @return String
     */
    public static String date2String(Date date, String pattern) {
        if (date == null || pattern == null) {
            return null;
        }
        return new SimpleDateFormat(pattern).format(date);
    }

    /**
     * 将String类型转换成Date类型
     *
     * @param date Date对象
     * @return
     */
    public static Date string2Date(String date) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 将String类型转换成Date类型
     *
     * @param date    Date对象
     * @param pattern 日期类型
     * @return String 返回类型
     * @author huangsm
     * @date 2016-11-30
     */
    public static Date string2Date(String date, String pattern) {
        if (date == null || pattern == null) {
            return null;
        }
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * 日期格式转换20111101000000，转换为2011-11-01 00:00:00
     *
     * @return String
     */
    public static String dateFormat(String str) {
        try {
            Date date = MyDateUtil.string2Date(str, MyDateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
            return MyDateUtil.date2String(date, MyDateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        } catch (Exception e) {
            return str;
        }
    }

    public static String getCurDate() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
        return df.format(new Date());
    }

    public static String getCurDateByFormat(String format, String date) {
        SimpleDateFormat df = new SimpleDateFormat(format);// 设置日期格式
        if (date == null) {
            return df.format(new Date());
        } else {
            return df.format(string2Date(date));
        }
    }


    /*
     * 根据年月获取月初
     * add by weiql 2017-05-13
     */
    public static String getBeginTime(int year, int month) {
        return year + "-" + month + "-" + "01 00:00:00";
    }

    /*
     * 根据年月获取月末
     * add by weiql 2017-05-13
     */
    public static String getEndTime(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        int maxDate = cal.getActualMaximum(Calendar.DATE);
        return year + "-" + month + "-" + maxDate + " 23:59:59";
    }

    //获取当前日期
    public static String getCurrentTimeYMD() {
        Format format = new SimpleDateFormat(DATE_PATTERN.YYYY_MM_DD);
        String temp = format.format(new Date());
        return temp;
    }

    public static Timestamp getDateAsTimestamp() {
        return new Timestamp(new Date().getTime());
    }

    public static String getCurrTimeYYYYMMDDHHMMSS() {
        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        return formatter.format(date);
    }

    public static Timestamp getNextMonthStartDate(Date date) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.set(5, 1);
        rightNow.set(11, 0);
        rightNow.set(14, 0);
        rightNow.set(13, 0);
        rightNow.set(12, 0);
        rightNow.set(2, rightNow.get(2) + 1);
        return new Timestamp(rightNow.getTimeInMillis());
    }

    public static Timestamp getCurrentMonthFirstDate(Date date) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.set(5, 1);
        rightNow.set(11, 0);
        rightNow.set(14, 0);
        rightNow.set(13, 0);
        rightNow.set(12, 0);
        rightNow.set(2, rightNow.get(2));
        return new Timestamp(rightNow.getTimeInMillis());
    }

    public static Timestamp geTimestamp(String time, String pattern) {
        Date date = string2Date(time, pattern);
        return new Timestamp(date.getTime());
    }

    public static Timestamp getMaxExpire() {
        Calendar cal = Calendar.getInstance();
        cal.set(2099, 11, 31, 0, 0, 0);
        Timestamp expireTime = new Timestamp(cal.getTimeInMillis());
        return expireTime;
    }

    public static final boolean middle(Date aValidDate, Date aTargetDate, Date aExpiredate) {
        return aValidDate.compareTo(aTargetDate) < 0 && aExpiredate.compareTo(aTargetDate) > 0 ? true : false;
    }

    public static Timestamp getCurrentDate() {
        //return ServiceManager.getOpDateTime();
        return new Timestamp(new Date().getTime());
    }

    public static String getNowTime() {
        long time = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(new Date(time));
    }

    /**
     * 获取日
     *
     * @param time
     * @return
     * @author cuiwl
     * @date Oct 24, 2014
     */
    public static int getDayOfMonth(long time) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTimeInMillis(time);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    /**
     * 获取当月最后一天
     *
     * @return
     */
    public static int getCurrentMonthLastDay() {
        Calendar a = Calendar.getInstance();
        a.set(Calendar.DATE, 1);// 把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);// 日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        return maxDate;
    }

    /**
     * 获取某个时间之前的月份
     *
     * @param date
     * @param i
     * @return
     */
    public static Date getNowInBeforeMonth(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, -i);
        return cal.getTime();
    }

    /**
     * 获取某个时间当月第一天的0时0分0秒
     *
     * @param timeOfMonth 当月随便某个时间
     * @return
     */
    public static long getStartTimeInMonth(long timeOfMonth) {
        Calendar cal = Calendar.getInstance();
        long afterClearTime = clearHHMMSS(timeOfMonth);
        cal.clear();
        cal.setTimeInMillis(afterClearTime);
        int firstDay = cal.getActualMinimum(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.DAY_OF_MONTH, firstDay);
        return cal.getTimeInMillis();
    }

    /**
     * 时分秒清零
     *
     * @param time
     * @return
     */
    public static long clearHHMMSS(long time) {
        Calendar cal = Calendar.getInstance();
        cal.clear();
        cal.setTimeInMillis(time);

        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);

        return cal.getTimeInMillis();
    }

    /**
     * 转换为时间戳
     *
     * @param strtime
     * @param datepattern
     * @return
     */
    public static Timestamp string2Timestamp(String strtime, String datepattern) {
        Date date = string2Date(strtime, datepattern);
        if (date != null) {
            return new Timestamp(date.getTime());
        }

        return null;
    }

    /**
     * 获取去年今日
     *
     * @return java.util.Date
     */
    public static Date getLastYearToday() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 把日期往前减少一年.整数往后推,负数往前移动
        calendar.add(Calendar.YEAR, -1);
        date = calendar.getTime();
        return date;
    }

    /**
     * 获取当前日期下个月第一天
     *
     * @return String
     */
    public static String getNextMonthFirstDate() {
        SimpleDateFormat dft = new SimpleDateFormat("yyyyMMdd");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        return dft.format(calendar.getTime());
    }

    /**
     * 获取6个月前的日期
     *
     * @return
     */
    public static Date getSixMonthsAgo() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 把日期往前减少6个月.整数往后推,负数往前移动
        calendar.add(Calendar.MONTH, -6);
        date = calendar.getTime();
        return date;
    }

    /**
     * 获取12+1个月前的日期
     *
     * @return
     */
    public static Date getThirteenMonthsAgo() {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 把日期往前减少13个月.整数往后推,负数往前移动
        calendar.add(Calendar.MONTH, -13);
        date = calendar.getTime();
        return date;
    }

    /**
     * 获得当前日期指定分钟数之前或之后的日期
     *
     * @param minutes 指定的分钟数 想要获得数分钟前的日期则输入负数，反之则输入正数
     * @return java.util.Date
     */
    public static Date getDateByMinutes(int minutes) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        date = calendar.getTime();
        return date;
    }

    /**
     * 获得指定日期指定分钟数之前或之后的日期
     *
     * @param date    指定日期
     * @param minutes 指定的分钟数 想要获得数分钟前的日期则输入负数，反之则输入正数
     * @return java.util.Date
     */
    public static Date changeDateByMinutes(Date date, int minutes) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MINUTE, minutes);
        return calendar.getTime();
    }

    /**
     * 获得指定日期指定小时数之前或之后的日期
     *
     * @param date 指定日期
     * @param hour 指定的小时数 想要获得数小时前的日期则输入负数，反之则输入正数
     * @return java.util.Date
     */
    public static Date changeDateByHour(Date date, int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR_OF_DAY, hour);
        return calendar.getTime();
    }

    /**
     * 获得指定日期指定天数之前或之后的日期
     *
     * @param date 指定日期
     * @param day  指定的天数 想要获得数天前的日期则输入负数，反之则输入正数
     * @return java.util.Date
     */
    public static Date changeDateByDay(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    public static String getMinusFormatTime(String s) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(s);
        calendar.add(Calendar.HOUR_OF_DAY, -12);
        return sdf.format(calendar.getTime());
    }

    /**
     * 日期减i月
     * 输入yyyymm
     * 输出yyyymm
     * return String
     */
    public static String dateMinusMonth(String str, int i) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
        Date dt = sdf.parse(str);//将字符串生成Date
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(dt);//使用给定的 Date 设置此 Calendar 的时间。
        rightNow.add(Calendar.MONTH, -i);// 日期减i个月
        Date dt1 = rightNow.getTime();//返回一个表示此 Calendar 时间值的 Date 对象。
        String reStr = sdf.format(dt1);//将给定的 Date 格式化为日期/时间字符串，并将结果添加到给定的 StringBuffer。
        return reStr;
    }

    /**
     * 改变事件格式
     * 如：String yyyymm
     * 转换为
     * String yyyy-mm返回
     */
    public static String dateChange(String time) throws Exception {

        String finalTime = time.substring(0, 4) + '年' + time.substring(4) + '月';

        return finalTime;
    }

    /**
     * 获取某一时间的 00:00:00 点 或者 23:59:59
     *
     * @param flag 标志位，用于指定获取开始时间还是结束时间。"start" 表示获取开始时间，其他值表示获取结束时间。
     * @param times 以天为单位的时间偏移量，用于计算目标时间。正值表示未来的时间，负值表示过去的时间。
     * @return 返回格式化后的开始时间或结束时间字符串。
     */
    public static String getStartOrEndTime(String flag, int times) {
        String time = new String();
        // 定义开始时间和结束时间的格式
        SimpleDateFormat startDf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        SimpleDateFormat endDf = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
        // 获取当前时间并根据 times 偏移
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, times);
        long l_date = cal.getTimeInMillis();
        Date date = new Date(l_date);
        // 根据 flag 判断是获取开始时间还是结束时间
        if (flag.equals("start")) {
            time = startDf.format(date); // 格式化开始时间
        } else {
            time = endDf.format(new Date()); // 格式化当前时间作为结束时间
        }

        return time;
    }

    /**
     * 获取当前时间i个月前的日期
     *
     * @return
     */
    public static String getCerMonthsAgo(int i) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        // 把日期往前减少6个月.整数往后推,负数往前移动
        calendar.add(Calendar.MONTH, -i);
        date = calendar.getTime();
        String time = df.format(date);
        return time;
    }

    /**
     * 获取当前时间年月
     *
     * @return 2023_04
     */
    public static String getCerYearMonth() {
        Calendar calendar = Calendar.getInstance();
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        int month = calendar.get(Calendar.MONTH) + 1;
        String monthStr = "";
        if (month < 10) {
            monthStr = "0" + String.valueOf(month);
        } else {
            monthStr = String.valueOf(month);
        }
        return year + "_" + monthStr;
    }

    public static String getStartOfDay(Date time, String patternMonth, String patternDayTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1); //把日期设置为当月第一天
        calendar.roll(Calendar.DATE, -1); //日期回滚一天，也就是最后一天
        calendar.setTime(time);
        SimpleDateFormat sdf = new SimpleDateFormat(patternMonth);
        String startTime = sdf.format(time) + patternDayTime;
        return startTime;
    }

    public static String getEndOfDay(Date time, String patternMonth, String patternDayTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.set(Calendar.DATE, 1); //把日期设置为当月第一天
        calendar.roll(Calendar.DATE, -1); //日期回滚一天，也就是最后一天
        //当月有多少天
        int maxDate = calendar.get(Calendar.DATE);
        SimpleDateFormat sdf = new SimpleDateFormat(patternMonth);
        String endTime = sdf.format(time) + maxDate + patternDayTime;
        return endTime;
    }

    public static String getNextBeforeMonth(Date date, String patteron, int days) {
        Calendar rightNow = Calendar.getInstance();
        rightNow.setTime(date);
        rightNow.add(Calendar.MONTH, days);
        return MyDateUtil.date2String(rightNow.getTime(), patteron);
    }


    /**
     * 根据两个时间获取相差的月数
     *
     * @param d1 日期起期
     * @param d2 日期止期
     * @return
     */
    public static int getDiffMonth(Date d1, Date d2) {
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(d1);
        c2.setTime(d2);
        int year1 = c1.get(Calendar.YEAR);
        int year2 = c2.get(Calendar.YEAR);
        int month1 = c1.get(Calendar.MONTH);
        int month2 = c2.get(Calendar.MONTH);
        int day1 = c1.get(Calendar.DAY_OF_MONTH);
        int day2 = c2.get(Calendar.DAY_OF_MONTH);
        // 获取年的差值
        int yearInterval = year1 - year2;
        // 如果 d1的 月-日 小于 d2的 月-日 那么 yearInterval-- 这样就得到了相差的年数
        if (month1 < month2 || month1 == month2 && day1 < day2) {
            yearInterval--;
        }
        // 获取月数差值
        int monthInterval = (month1 + 12) - month2;
        if (day1 < day2) {
            monthInterval--;
        }
        monthInterval %= 12;
        int monthsDiff = Math.abs(yearInterval * 12 + monthInterval);
        return monthsDiff;
    }

    /**
     * 获取两个时间的天、小时、分钟、秒差
     *
     * @param startDate
     * @param endDate
     * @param type
     * @return
     */
    public static long getDateTimePoor (Date startDate, Date endDate, String type){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String newDate = df.format(startDate);
        String sqlDate1 = df.format(endDate);
        try {
            startDate = df.parse(newDate);
            endDate = df.parse(sqlDate1);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Long time =  endDate.getTime();
        Long time2 =  startDate.getTime();
        switch (type) {
            case "dd":
                return Math.abs((time - time2) / (24 * 3600 * 1000));
            case "hh":
                return Math.abs(time - time2) / (1000 * 60 * 60);
            case "mm":
                return Math.abs(time - time2) / (1000 * 60);
            case "ss":
                return Math.abs(time - time2) / (1000);
            default:
                return 0;
        }
    }

    /**
     * 获取当前指定时间月的第一天
     *
     * @param time
     * @param patternMonth
     * @param patternDayTime
     * @return
     */
    public static String getStartOfMonth(Date time, String patternMonth, String patternDayTime){
        Calendar calendar=Calendar.getInstance();
        calendar.set(Calendar.DATE,1); //把日期设置为当月第一天
        calendar.roll(Calendar.DATE, -1); //日期回滚一天，也就是最后一天
        calendar.setTime(time);
        SimpleDateFormat sdf = new SimpleDateFormat(patternMonth);
        String startTime = sdf.format(time) + patternDayTime;
        return startTime;
    }

    /**
     * 获取当前指定时间月的最后一天
     *
     * @param time
     * @param patternMonth
     * @param patternDayTime
     * @return
     */
    public static String getEndOfMonth(Date time, String patternMonth, String patternDayTime){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        calendar.set(Calendar.DATE,1); //把日期设置为当月第一天
        calendar.roll(Calendar.DATE, -1); //日期回滚一天，也就是最后一天
        //当月有多少天
        int maxDate = calendar.get(Calendar.DATE);
        SimpleDateFormat sdf = new SimpleDateFormat(patternMonth);
        String endTime = sdf.format(time) + maxDate + patternDayTime;
        return endTime;
    }

    /**
     * 获取指定日期是当前月份的第几天。
     *
     * @param date 指定的日期，类型为Date。
     * @return 返回指定日期是当前月份的第几天，返回值为String类型。
     */
    public static String getCurrentTimeOfDay(Date date) {
        // 获取当前时间的日历实例
        Calendar calendar = Calendar.getInstance();
        // 设置日历时间为传入的日期
        calendar.setTime(date);

        // 打印出具体时间（该行代码实际作用于输出，非函数功能需求，可忽略）
        System.out.println(calendar.getTime());
        // 返回传入日期是当前月份的第几天
        return String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }
}

