package com.azh.dateutils;

import cn.hutool.core.map.MapUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import io.swagger.models.auth.In;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.azh.dateutils.MyDateUtil.getStartOrEndTime;

public class DateUtilsDemo {
    public static void main(String[] args) {
        Date currDate = MyDateUtil.string2Date("2022-08-24", MyDateUtil.DATE_PATTERN.YYYY_MM_DD);
        Date invldTime = MyDateUtil.string2Date("2024-02-04 15:17:02", MyDateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
        Date effTime = MyDateUtil.string2Date("2022-08-26", MyDateUtil.DATE_PATTERN.YYYY_MM_DD);
        if (currDate.after(invldTime) || currDate.equals(effTime)) {
            System.out.println("当前时间大于生效时间");
        }

        if (currDate.before(effTime)) {
            System.out.println("当前时间在失效时间之前");
        }
        String endOfDay = MyDateUtil.getEndOfDay(invldTime, "yyyyMM", "235959");
        String startOfDay = MyDateUtil.getStartOfDay(invldTime, "yyyyMM", "01000000");
        System.out.println(endOfDay);
        System.out.println(startOfDay);

        long currDateTime = currDate.getTime();
        long endSubTime = effTime.getTime();
        long day = (endSubTime-currDateTime)/24/3600/1000;

        Date tempDate = currDate;
        for (long i=0; i<day; i++) {
            tempDate = MyDateUtil.changeDateByDay(tempDate, 1);
            System.out.println(tempDate);
            System.out.println(currDate);
        }
        System.out.println(day);

        Date date = MyDateUtil.string2Date("2024-02-15", MyDateUtil.DATE_PATTERN.YYYY_MM_DD);
        Date date2 = MyDateUtil.string2Date("2024-03-01", MyDateUtil.DATE_PATTERN.YYYY_MM_DD);
        long monthNum = MyDateUtil.getDiffMonth(date, date2);
        System.out.println(monthNum);

        String nextMonth = MyDateUtil.getNextBeforeMonth(date2, MyDateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS, -1);
        System.out.println(nextMonth);

        Date date3 = MyDateUtil.string2Date("20240204151702", MyDateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
        Date date4 = MyDateUtil.string2Date("20240204161703", MyDateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
        long ss = MyDateUtil.getDateTimePoor(date3, date4, "ss");
        System.out.println(ss);


        String str2 = "2024-03-17 00:00:00";
        String str3 = "2024-03-17 23:59:59";
        Map<String, Object> paramsMap = new HashMap<>();
        Date time = MyDateUtil.string2Date(str2, MyDateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
        paramsMap.put("startDate", MyDateUtil.getStartOfMonth(time, "yyyyMM", "01000000"));
        paramsMap.put("endDate", MyDateUtil.getEndOfMonth(time, "yyyyMM", "235959"));

        String nextOrBeforeMonth = MyDateUtil.getNextBeforeMonth(time, MyDateUtil.DATE_PATTERN.YYYYMM, -1);
        Date date5 = MyDateUtil.string2Date(nextOrBeforeMonth, MyDateUtil.DATE_PATTERN.YYYYMMDDHHMMSS);
        paramsMap.put("startDate", MyDateUtil.getStartOfMonth(date, "yyyyMM", "01000000"));
        paramsMap.put("endDate", MyDateUtil.getEndOfMonth(date, "yyyyMM", "235959"));
        System.out.println(JSON.toJSONString(paramsMap));


        String start = getStartOrEndTime("start", 0);
        System.out.println(start);
        
        String dateStr = "Tue Mar 12 10:00:40 CST 2024";
        // 创建SimpleDateFormat对象，使用美国的日期时间格式和语言规则
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.US);
        try {
            // 将日期字符串解析为Date对象
            // CST 在mac 和 win 系统解析出来的时间不一致
            Date date6 = sdf.parse(dateStr);
            // 输出解析后的日期
            System.out.println(date6);
            String s = MyDateUtil.date2String(date6, MyDateUtil.DATE_PATTERN.YYYY_MM_DD_HH_MM_SS);
            System.out.println(s);
        } catch (ParseException e) {
            // 处理解析异常
            e.printStackTrace();
        }
    }
}
