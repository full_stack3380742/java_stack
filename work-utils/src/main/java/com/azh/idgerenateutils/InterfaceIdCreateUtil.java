package com.azh.idgerenateutils;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.XmlUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class InterfaceIdCreateUtil {
    private static final ObjectMapper objectMapper = new ObjectMapper();
    // 根据渠道code和子渠道code生成id
    public static String channelIdGenerate(String channelCode, String subChannelCode) {
        long id = (channelCode + subChannelCode).hashCode() & Long.MAX_VALUE;
        return String.valueOf(id);
    }

    // 根据 url 生成对应接口的id
    public static String plaInterfaceId (String url) {
        long id = url.hashCode() & Long.MAX_VALUE;
        return String.valueOf(id);
    }

    // 根据提供的接口服务类名和服务编码生成对应接口的id
    public static String outInterfaceId (String className, String outCode) {
        long id = (className + outCode).hashCode() & Long.MAX_VALUE;
        return String.valueOf(id);
    }

    public static Map<String,Object> buildRespOut(String str) throws JsonProcessingException {
        Map map = objectMapper.readValue(str, Map.class);
        if (MapUtil.isEmpty(map)) {
            return new HashMap<>();
        }
        Map object = objectMapper.readValue(String.valueOf(map.get("object")), Map.class);
        if (MapUtil.isEmpty(object)) {
            return new HashMap<>();
        }
        Map result = objectMapper.readValue(String.valueOf(object.get("result")), Map.class);
        if (MapUtil.isEmpty(result)) {
            return new HashMap<>();
        }
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("authCode", result.get("authCode"));
        resultMap.put("authCode", result.get("authCode"));
        resultMap.put("authCode", result.get("authCode"));
        resultMap.put("cgi", result.get("authCode"));
        return map;
    }

    public static void main(String[] args) throws JsonProcessingException {
         String channelId = InterfaceIdCreateUtil.channelIdGenerate("ngbusi", "ngbusi");
        String plaChannelId = InterfaceIdCreateUtil.plaInterfaceId("/crm/smsSendInfoBossHisQuery");
        String outChannelId = InterfaceIdCreateUtil.outInterfaceId("BjCommonOutService", "OPF_REMIND_RES_BOSS_HIS_QUERY_001");
        System.out.println("channelId = " + channelId  + "\n" + "outChannelId = " + outChannelId + "\n" + "plaChannelId = " + plaChannelId);

        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" +
                "<RETRUN_INFO><x_RECORDNUM>2</x_RECORDNUM><retInfo><ACCEPTTIME>2023-10-31 17:47:53</ACCEPTTIME><MSG_CONTENT>cxll</MSG_CONTENT><DEST_ID>10086</DEST_ID></retInfo><retInfo><ACCEPTTIME>2023-10-31 17:49:07</ACCEPTTIME><MSG_CONTENT>7777</MSG_CONTENT><DEST_ID>10086</DEST_ID></retInfo><x_RESULTCODE>0000</x_RESULTCODE><x_RESULTINFO>成功</x_RESULTINFO></RETRUN_INFO>";
        String toJSONString = JSON.toJSONString(XmlUtil.xmlToMap(xmlStr));
        System.out.println(toJSONString);
    }
}
