package com.azh.testdemo;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class TestDemo {
    public static void main(String[] args) {
        for (int i=1; i<=31; i++) {
            if (i<10) {
                String str = "alter table bs_broadband_satisfaction_fix_0" + i + " rename to zxdba_bak.20240418_bs_broadband_satisfaction_fix_0" + i + ";";
                System.out.println(str);
            }else {
                String str = "alter table bs_broadband_satisfaction_fix_" + i + " rename to zxdba_bak.20240418_bs_broadband_satisfaction_fix_" + i + ";";
                System.out.println(str);
            }
        }
    }
}
