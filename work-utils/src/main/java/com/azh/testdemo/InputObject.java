package com.azh.testdemo;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class InputObject {
    Map<String, Object> params = new HashMap<>();
}
