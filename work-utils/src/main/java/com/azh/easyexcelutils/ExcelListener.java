package com.azh.easyexcelutils;

import cn.hutool.core.date.DateUtil;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//创建读取excel监听器
public class ExcelListener extends AnalysisEventListener<ReadData> {

    //创建list集合封装最终的数据
    List<ReadData> list = new ArrayList<ReadData>();

    //一行一行去读取excle内容
    @Override
    public void invoke(ReadData user, AnalysisContext analysisContext) {
        //将读取到的数据输出到控制台
        list.add(user);
    }

    //读取完成后执行
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        Map<String, String> map = new HashMap<>();
        StringBuffer buffstr = new StringBuffer("insert into bs_static_config (`CONFIG_TYPE_CD`, `FST_CONFIG_ITEM_NM`, `SECD_CONFIG_ITEM_NM`, `RMK`, `CRT_TIME`, `STS_CD`, `CONFIG_ID`) values ");
        int count = 0;
        Date date = new Date();
        String yyyyMMdd = DateUtil.format(date, "yyyyMMdd");
        String format = DateUtil.format(date, "yyyy-MM-dd HH:mm:ss");
        for (ReadData readData : list) {
            count++;
            map.put(readData.getEnumId(), readData.getEnumName());
            buffstr.append("(")
                    .append("'ELECTRONIC_CHANNEL_ID'").append(",")
                    .append("'" + readData.getChannelId() + "'").append(",")
                    .append("'" + readData.getName() + "'").append(",")
                    .append("'电子发票枚举值'").append(",")
                    .append("'" + format + "'").append(",")
                    .append("'1'").append(",")
                    .append("'CHANNEL_ID_" + yyyyMMdd +  count + "'")
                    .append(")").append(",");
        }

        String resultDataJson = JSON.toJSONString(map);
        System.out.println(resultDataJson);
        System.out.println(map.size());
        System.out.println(buffstr);
    }
}
