package com.azh.easyexcelutils;

import com.alibaba.excel.EasyExcel;

public class TestExcelDemo {
    public static void main(String[] args) {
        //定义要读取的文件路径及名称
        String fileName = "/Users/azh/Downloads/亚信资料/项目资料/Zhe_Jiang_ZX/需求文档/20240129-关于电子发票菜单优化的需求/设计文档/channelidAndName.xlsx";

        //执行读取操作
        EasyExcel.read(fileName, ReadData.class, new ExcelListener()).sheet().doRead();
    }
}
