package com.azh.easyexcelutils;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * Excel对应列名称
 */

@Getter
@Setter
public class ReadData {
    //设置列对应的属性
    @ExcelProperty("受理渠道编码")
    private String channelId;

    //设置列对应的属性
    @ExcelProperty("受理渠道名称")
    private String name;

    @ExcelProperty("枚举值编码")
    private String enumId;

    @ExcelProperty("枚举值名称")
    private String enumName;

    @Override
    public String toString() {
        return "ReadData{" +
                "channelId=" + channelId +
                ", name='" + name + '\'' +
                ", enumId='" + enumId + '\'' +
                ", enumName='" + enumName + '\'' +
                '}';
    }
}
