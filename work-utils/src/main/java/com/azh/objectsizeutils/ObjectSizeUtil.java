package com.azh.objectsizeutils;

import org.apache.lucene.util.RamUsageEstimator;

import java.util.HashMap;
import java.util.Map;

public class ObjectSizeUtil {
    /**
     * 获取对象的内存大小。
     * 该方法打印出给定对象的内存占用大小，以字节和可读性更强的格式显示。
     *
     * @param object 需要计算内存大小的对象。
     */
    public static void getObjectSize(Object object) {
        // 打印对象的字节大小
        System.out.println("map size 100, value is " + RamUsageEstimator.sizeOf(object));

        // 打印对象的可读性更强的内存大小（如KB, MB等）
        System.out.println("map size 100, value is " + RamUsageEstimator.humanSizeOf(object));
    }



    public static void main(String[] args) {
        Map<String, Object> jobPlanMap = new HashMap<>();
        // 主键
        jobPlanMap.put("id", "261231644319420819");
        // 开始时间
        jobPlanMap.put("broadBandAccount", "hztla88143981");
        // 结束时间
        jobPlanMap.put("contactNumber", "15144441234");
        // 状态 0：表示此计划任务未执行 1：表示此计划任务状态已经完成
        jobPlanMap.put("prefecture", "内蒙古自治区巴彦淖尔市");
        jobPlanMap.put("district", "五原县塔尔湖镇");
        // 创建时间
        jobPlanMap.put("outboundCallTime", "2024-04-01 07:59:59");
        jobPlanMap.put("callbackTimeOutboundCallResult", "2024-04-01 07:59:59");
        // 更新时间
        jobPlanMap.put("outboundCallResult", "1");
        jobPlanMap.put("pushStatus", "1");
        String broadband = "1";
        getObjectSize(jobPlanMap);

        // 1.5 KB
    }
}
