package com.sftpftp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan(basePackages = {"com.sftpftp.jschapi.**.mapper"})
public class FTPSftpApplication {
    public static void main(String[] args) {
        SpringApplication.run(FTPSftpApplication.class, args);

    }
}
