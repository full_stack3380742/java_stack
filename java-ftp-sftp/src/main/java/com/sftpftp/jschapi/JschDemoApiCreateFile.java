package com.sftpftp.jschapi;

import cn.hutool.core.date.StopWatch;
import com.jcraft.jsch.ChannelSftp;
import com.sftpftp.common.SFTPUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

/**
 * 使用Jsch库提供的api连接ssh服务器，不支持ftp服务器连接
 * 支持 sftp 连接
 */
@Slf4j
public class JschDemoApiCreateFile {
    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String host = "localhost";
        String user = "sftp";
        String password = "sftp@sftp";

        try {
            //ChannelSftp sftp = SFTPUtils.getChannelSftp(user,password, host, 2222);
            // 将临时文件上传到FTP服务器
            //OutputStream outputStream = sftp.put("/upload/2024-04-01_外呼目标清单.csv", ChannelSftp.OVERWRITE);
            OutputStream outputStream = new FileOutputStream(new File("/Users/azh/Downloads/2024-04-01_外呼目标清单.csv"));
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, StandardCharsets.UTF_8));
            for (long i=0; i<200000; i++) {
                bufferedWriter.write("hztla88143981" + i + "|内蒙古自治区巴彦淖尔市" + i + "|五原县" + i);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SFTPUtils.disconnect();
        }
        stopWatch.stop();
        log.info("在线写入20w数据到sftp服务器的文件中，花费时间为：" + stopWatch.getTotalTimeMillis());
    }
}
