package com.sftpftp.jschapi.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 
 * 家宽满意度修复数据表
 */
public class BsBroadbandSatisfactionFixEntity implements Serializable {
    /**
     * 主键ID
     */
    private String id;

    /**
     * 宽带账号
     */
    private String broadbandAccount;

    /**
     * 联系电话
     */
    private String contactNumber;

    /**
     * 地市
     */
    private String regionId;

    /**
     * 区县
     */
    private String countyId;

    /**
     * 送外呼时间
     */
    private Date outCallTime;

    /**
     * 外呼结果回传时间
     */
    private Date outCallReceiveTime;

    /**
     * 推送状态 0:待推送 1:已推送 2:号码异常
     */
    private String pushStatus;

    /**
     * 外呼结果：0:未呼通，1:呼通
     */
    private String outCallResult;

    /**
     * 数据入库时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date cmosModifyTime;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBroadbandAccount() {
        return broadbandAccount;
    }

    public void setBroadbandAccount(String broadbandAccount) {
        this.broadbandAccount = broadbandAccount;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getCountyId() {
        return countyId;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    public Date getOutCallTime() {
        return outCallTime;
    }

    public void setOutCallTime(Date outCallTime) {
        this.outCallTime = outCallTime;
    }

    public Date getOutCallReceiveTime() {
        return outCallReceiveTime;
    }

    public void setOutCallReceiveTime(Date outCallReceiveTime) {
        this.outCallReceiveTime = outCallReceiveTime;
    }

    public String getPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(String pushStatus) {
        this.pushStatus = pushStatus;
    }

    public String getOutCallResult() {
        return outCallResult;
    }

    public void setOutCallResult(String outCallResult) {
        this.outCallResult = outCallResult;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getCmosModifyTime() {
        return cmosModifyTime;
    }

    public void setCmosModifyTime(Date cmosModifyTime) {
        this.cmosModifyTime = cmosModifyTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BsBroadbandSatisfactionFixEntity other = (BsBroadbandSatisfactionFixEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getBroadbandAccount() == null ? other.getBroadbandAccount() == null : this.getBroadbandAccount().equals(other.getBroadbandAccount()))
            && (this.getContactNumber() == null ? other.getContactNumber() == null : this.getContactNumber().equals(other.getContactNumber()))
            && (this.getRegionId() == null ? other.getRegionId() == null : this.getRegionId().equals(other.getRegionId()))
            && (this.getCountyId() == null ? other.getCountyId() == null : this.getCountyId().equals(other.getCountyId()))
            && (this.getOutCallTime() == null ? other.getOutCallTime() == null : this.getOutCallTime().equals(other.getOutCallTime()))
            && (this.getOutCallReceiveTime() == null ? other.getOutCallReceiveTime() == null : this.getOutCallReceiveTime().equals(other.getOutCallReceiveTime()))
            && (this.getPushStatus() == null ? other.getPushStatus() == null : this.getPushStatus().equals(other.getPushStatus()))
            && (this.getOutCallResult() == null ? other.getOutCallResult() == null : this.getOutCallResult().equals(other.getOutCallResult()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getCmosModifyTime() == null ? other.getCmosModifyTime() == null : this.getCmosModifyTime().equals(other.getCmosModifyTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getBroadbandAccount() == null) ? 0 : getBroadbandAccount().hashCode());
        result = prime * result + ((getContactNumber() == null) ? 0 : getContactNumber().hashCode());
        result = prime * result + ((getRegionId() == null) ? 0 : getRegionId().hashCode());
        result = prime * result + ((getCountyId() == null) ? 0 : getCountyId().hashCode());
        result = prime * result + ((getOutCallTime() == null) ? 0 : getOutCallTime().hashCode());
        result = prime * result + ((getOutCallReceiveTime() == null) ? 0 : getOutCallReceiveTime().hashCode());
        result = prime * result + ((getPushStatus() == null) ? 0 : getPushStatus().hashCode());
        result = prime * result + ((getOutCallResult() == null) ? 0 : getOutCallResult().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getCmosModifyTime() == null) ? 0 : getCmosModifyTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", broadbandAccount=").append(broadbandAccount);
        sb.append(", contactNumber=").append(contactNumber);
        sb.append(", regionId=").append(regionId);
        sb.append(", countyId=").append(countyId);
        sb.append(", outCallTime=").append(outCallTime);
        sb.append(", outCallReceiveTime=").append(outCallReceiveTime);
        sb.append(", pushStatus=").append(pushStatus);
        sb.append(", outCallResult=").append(outCallResult);
        sb.append(", createTime=").append(createTime);
        sb.append(", cmosModifyTime=").append(cmosModifyTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}