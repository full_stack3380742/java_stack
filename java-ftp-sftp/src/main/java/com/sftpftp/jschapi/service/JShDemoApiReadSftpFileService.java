package com.sftpftp.jschapi.service;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.StopWatch;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.azh.common.NamedThreadFactory;
import com.azh.dateutils.MyDateUtil;
import com.jcraft.jsch.ChannelSftp;
import com.sftpftp.common.JDBCUtils;
import com.sftpftp.common.SFTPUtils;
import com.sftpftp.jschapi.entity.BsBroadbandSatisfactionFixEntity;
import com.sftpftp.jschapi.mapper.BsBroadbandSatisfactionFixMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class JShDemoApiReadSftpFileService {

    private final BsBroadbandSatisfactionFixMapper bsBroadbandSatisfactionFixMapper;

    @Autowired
    public JShDemoApiReadSftpFileService(BsBroadbandSatisfactionFixMapper bsBroadbandSatisfactionFixMapper) {
        this.bsBroadbandSatisfactionFixMapper = bsBroadbandSatisfactionFixMapper;
    }

    protected static ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors()
            , Runtime.getRuntime().availableProcessors() * 2 - 1, 0, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(1000),
            new NamedThreadFactory("readSftpFile"),
            (r, executor)->{
                if (!executor.isShutdown()) {
                    try {
                        // 主线程将会被阻塞
                        log.info("线程队列此时大小为：{}", executor.getQueue().size());
                        executor.getQueue().put(r);
                    } catch (InterruptedException e) {
                        // should not be interrupted
                    }
                }
            });

    public boolean readSftpFile() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String host = "localhost";
        String user = "sftp";
        String password = "sftp@sftp";

        try {
            ChannelSftp sftp = SFTPUtils.getChannelSftp(user,password, host, 2222);
            // TODO: Handle your FTP operations here...
            InputStream inputStream = sftp.get("/upload/2024-04-01_外呼目标清单.csv");
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                String str;
                List<BsBroadbandSatisfactionFixEntity> mapList = new ArrayList<>();
                long pushCount = 100000;
                while (StringUtils.isNotEmpty(str = bufferedReader.readLine()) && pushCount >= 1000){
                    BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity = buildLineData(str);
                    mapList.add(bsBroadbandSatisfactionFixEntity);
                    if (mapList.size() == 1000) {
                        List<BsBroadbandSatisfactionFixEntity> bsBroadbandSatisfactionFixEntityList = new ArrayList<>();
                        bsBroadbandSatisfactionFixEntityList.addAll(mapList);
                        CompletableFuture.runAsync(() -> convertDbByMybatis(bsBroadbandSatisfactionFixEntityList), threadPoolExecutor);
                        //CompletableFuture.runAsync(() -> convertDbByJDBCBatch(homeBroadbandSatisfactionDataList), threadPoolExecutor);
                        pushCount -= 1000;
                        mapList.clear();
                        log.info("pushCount:{}", pushCount);
                    }
                }
                if(CollUtil.isNotEmpty(mapList)) {
                    log.info("处理集合中剩余的数据：{}", mapList.size());
                    CompletableFuture.runAsync(() -> convertDbByMybatis(mapList), threadPoolExecutor);
                    //CompletableFuture.runAsync(() -> convertDbByJDBCBatch(mapList), threadPoolExecutor);
                    mapList.clear();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SFTPUtils.disconnect();
        }
        stopWatch.stop();
        log.info("在线从ftp服务器上读取20w数据话费时间为：" + stopWatch.getTotalTimeMillis() + "ms");
        return true;
    }

    private void convertDbByJDBCBatch(List<BsBroadbandSatisfactionFixEntity> bsBroadbandSatisfactionFixEntityList) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String url = "jdbc:mysql://localhost:3306/mysql_dev?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true";
        String user = "root";
        String password = "123456";
        int[] ints = null;
        PreparedStatement preparedStatement = null;
        try (Connection connection = JDBCUtils.getConnection(url, user, password);){
            String day = MyDateUtil.getCurrentTimeOfDay(new Date());
            // ToDo: 去掉一些不必要的字段
            String sql = "insert into bs_broadband_satisfaction_fix (id, broadband_account, contact_number, \n" +
                    "      region_id, county_id, out_call_time, \n" +
                    "      out_call_receive_time, push_status, out_call_result, \n" +
                    "      create_time, cmos_modify_time)\n" +
                    "    values (?,?,?,?,?,?,?,?,?,?,?)";
            preparedStatement = connection.prepareStatement(sql);
            connection.setAutoCommit(false);
            for (BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity : bsBroadbandSatisfactionFixEntityList) {
                preparedStatement.setString(1, IdUtil.getSnowflakeNextIdStr());
                preparedStatement.setString(2, bsBroadbandSatisfactionFixEntity.getBroadbandAccount());
                preparedStatement.setString(3, RandomUtil.randomString(11));
                preparedStatement.setString(4, bsBroadbandSatisfactionFixEntity.getRegionId());
                preparedStatement.setString(5, bsBroadbandSatisfactionFixEntity.getCountyId());
                preparedStatement.setObject(6, new Date());
                preparedStatement.setString(7, "0");
                preparedStatement.setString(8, "0");
                preparedStatement.setString(9, "0");
                preparedStatement.setObject(10, new Date());
                preparedStatement.setObject(11, new Date());
                preparedStatement.addBatch();
            }
            ints = preparedStatement.executeBatch();
            if (ints != null) {
                log.info("1000条数据插入结果为：{}", ints.length);
            }
            preparedStatement.clearBatch();
            connection.commit();
        } catch (SQLException e) {
            log.error("JDBC查询异常：", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        stopWatch.stop();
        log.info("JDBC批量模式插入1000条数据花费时间为：{}ms", stopWatch.getTotalTimeMillis());
    }


    /**
     * 根据输入的字符串构建行数据的映射。
     *
     * @param lineDataStr 以管道符('|')分隔的字符串，包含宽带账号、地市和区县信息。
     * @return 返回一个包含宽带账号、地市和区县信息的映射表。
     */
    private BsBroadbandSatisfactionFixEntity buildLineData(String lineDataStr) {
        String[] split = lineDataStr.split("\\|");
        BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity = new BsBroadbandSatisfactionFixEntity();
        // 分割输入字符串并填充映射表
        // 宽带账号
        bsBroadbandSatisfactionFixEntity.setBroadbandAccount(split[0]);
        // 地市
        bsBroadbandSatisfactionFixEntity.setRegionId(split[1]);
        // 区县
        bsBroadbandSatisfactionFixEntity.setCountyId(split[2]);

        return bsBroadbandSatisfactionFixEntity;
    }

    private void convertDbByMybatis(List<BsBroadbandSatisfactionFixEntity> bsBroadbandSatisfactionFixEntityList) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("批量插入数据条数为：{}", bsBroadbandSatisfactionFixEntityList.size());
        for (BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity : bsBroadbandSatisfactionFixEntityList) {
            bsBroadbandSatisfactionFixEntity.setOutCallTime(new Date());
            bsBroadbandSatisfactionFixEntity.setOutCallResult("0");
            bsBroadbandSatisfactionFixEntity.setOutCallReceiveTime(new Date());
            bsBroadbandSatisfactionFixEntity.setPushStatus("0");
            bsBroadbandSatisfactionFixEntity.setCreateTime(new Date());
            bsBroadbandSatisfactionFixEntity.setCmosModifyTime(new Date());
            bsBroadbandSatisfactionFixEntity.setContactNumber(RandomUtil.randomString(11));
            bsBroadbandSatisfactionFixEntity.setId(IdUtil.getSnowflakeNextIdStr());
        }
        int i = bsBroadbandSatisfactionFixMapper.insertBatch(bsBroadbandSatisfactionFixEntityList);
        log.info("批量插入数据成功条数为：{}", i);
        stopWatch.stop();
        log.info("使用mybatis动态拼接sql插入数据花费时间为：{}ms", stopWatch.getTotalTimeMillis());
    }
}
