package com.sftpftp.jschapi.controller;

import cn.hutool.core.date.StopWatch;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.azh.dateutils.MyDateUtil;
import com.sftpftp.common.JDBCUtils;
import com.sftpftp.jschapi.entity.BsBroadbandSatisfactionFixEntity;
import com.sftpftp.jschapi.mapper.BsBroadbandSatisfactionFixMapper;
import com.sftpftp.jschapi.service.JShDemoApiReadSftpFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@Slf4j
public class JschApiSftpCOntroller {
    private final BsBroadbandSatisfactionFixMapper bsBroadbandSatisfactionFixMapper;
    private final JShDemoApiReadSftpFileService jShDemoApiReadSftpFileService;

    @Autowired
    public JschApiSftpCOntroller(BsBroadbandSatisfactionFixMapper bsBroadbandSatisfactionFixMapper,
                                 JShDemoApiReadSftpFileService jShDemoApiReadSftpFileService) {
        this.bsBroadbandSatisfactionFixMapper = bsBroadbandSatisfactionFixMapper;
        this.jShDemoApiReadSftpFileService = jShDemoApiReadSftpFileService;
    }

    @GetMapping("/testselect")
    public BsBroadbandSatisfactionFixEntity testselect() {
        BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity = bsBroadbandSatisfactionFixMapper.selectByPrimaryKey("123456");
        log.info("获取到的数据为：{}", JSON.toJSONString(bsBroadbandSatisfactionFixEntity));
        return bsBroadbandSatisfactionFixEntity;
    }

    @GetMapping("/testinsert")
    public int testInsert() {
        BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity = new BsBroadbandSatisfactionFixEntity();
        bsBroadbandSatisfactionFixEntity.setId(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity.setBroadbandAccount(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity.setContactNumber(RandomUtil.randomString(11));
        bsBroadbandSatisfactionFixEntity.setRegionId("内蒙古");
        bsBroadbandSatisfactionFixEntity.setCountyId("五原县");
        bsBroadbandSatisfactionFixEntity.setOutCallTime(new Date());
        bsBroadbandSatisfactionFixEntity.setOutCallReceiveTime(new Date());
        bsBroadbandSatisfactionFixEntity.setPushStatus("0");
        bsBroadbandSatisfactionFixEntity.setOutCallResult("0");
        bsBroadbandSatisfactionFixEntity.setCreateTime(new Date());
        bsBroadbandSatisfactionFixEntity.setCmosModifyTime(new Date());
        int insert = bsBroadbandSatisfactionFixMapper.insert(bsBroadbandSatisfactionFixEntity);
        log.info("插入结果为：{}", insert);
        return insert;
    }

    @GetMapping("/batchinsert")
    public int batchInsert() {
        BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity = new BsBroadbandSatisfactionFixEntity();
        bsBroadbandSatisfactionFixEntity.setId(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity.setBroadbandAccount(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity.setContactNumber(RandomUtil.randomString(11));
        bsBroadbandSatisfactionFixEntity.setRegionId("内蒙古");
        bsBroadbandSatisfactionFixEntity.setOutCallResult("0");
        bsBroadbandSatisfactionFixEntity.setOutCallTime(new Date());
        bsBroadbandSatisfactionFixEntity.setCountyId("五原县");
        bsBroadbandSatisfactionFixEntity.setPushStatus("0");
        bsBroadbandSatisfactionFixEntity.setCreateTime(new Date());
        bsBroadbandSatisfactionFixEntity.setOutCallTime(new Date());
        bsBroadbandSatisfactionFixEntity.setCmosModifyTime(new Date());

        List<BsBroadbandSatisfactionFixEntity> dataList = new ArrayList<>();
        BsBroadbandSatisfactionFixEntity bsBroadbandSatisfactionFixEntity1 = new BsBroadbandSatisfactionFixEntity();

        bsBroadbandSatisfactionFixEntity1.setId(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity1.setBroadbandAccount(IdUtil.getSnowflakeNextIdStr());
        bsBroadbandSatisfactionFixEntity1.setContactNumber(RandomUtil.randomString(11));
        bsBroadbandSatisfactionFixEntity1.setRegionId("内蒙古");
        bsBroadbandSatisfactionFixEntity1.setOutCallResult("0");
        bsBroadbandSatisfactionFixEntity1.setOutCallTime(new Date());
        bsBroadbandSatisfactionFixEntity1.setCountyId("五原县");
        bsBroadbandSatisfactionFixEntity1.setPushStatus("0");
        bsBroadbandSatisfactionFixEntity1.setCreateTime(new Date());
        bsBroadbandSatisfactionFixEntity1.setOutCallReceiveTime(new Date());
        bsBroadbandSatisfactionFixEntity1.setCmosModifyTime(new Date());
        dataList.add(bsBroadbandSatisfactionFixEntity);
        dataList.add(bsBroadbandSatisfactionFixEntity1);
        int i = bsBroadbandSatisfactionFixMapper.insertBatch(dataList);
        log.info("批量插入结果为：{}", i);
        return i;
    }

    @GetMapping("/testreadfile")
    public void testReadFile() {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        jShDemoApiReadSftpFileService.readSftpFile();
        stopWatch.stop();
        log.info("20w 数据入库话费时间为：{}, ms", stopWatch.getTotalTimeMillis());
    }


    @GetMapping("/testjdbcselect")
    public BsBroadbandSatisfactionFixEntity testJdbcSelect() {
        String url = "jdbc:mysql://localhost:3306/mysql_dev?useUnicode=true&characterEncoding=utf8&allowMultiQueries=true&serverTimezone=GMT%2B8&rewriteBatchedStatements=true";
        String user = "root";
        String password = "123456";
        try (Connection connection = JDBCUtils.getConnection(url, user, password)) {
            String day = MyDateUtil.getCurrentTimeOfDay(new Date());
            String sql = "insert into bs_broadband_satisfaction_fix (id, broadband_account, contact_number, \n" +
                    "      region_id, county_id, out_call_time, \n" +
                    "      out_call_receive_time, push_status, out_call_result, \n" +
                    "      create_time, cmos_modify_time)\n" +
                    "    values (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            connection.setAutoCommit(false);
            for (int i=0; i<2; i++) {
                preparedStatement.setString(1, IdUtil.getSnowflakeNextIdStr());
                preparedStatement.setString(2, IdUtil.getSnowflakeNextIdStr());
                preparedStatement.setString(3, RandomUtil.randomString(11));
                preparedStatement.setString(4, "内蒙古");
                preparedStatement.setString(5, "五原县");
                preparedStatement.setObject(6, new Date());
                preparedStatement.setObject(7, new Date());
                preparedStatement.setString(8, "0");
                preparedStatement.setString(9, "0");
                preparedStatement.setObject(10, new Date());
                preparedStatement.setObject(11, new Date());

                preparedStatement.addBatch();
                int[] ints = preparedStatement.executeBatch();
                log.info("批量插入结果为：{}", JSON.toJSONString(ints));
                preparedStatement.clearBatch();
            }
            connection.commit();
        } catch (Exception e) {
            log.error("JDBC查询异常：", e);
        }
        return null;
    }
}
