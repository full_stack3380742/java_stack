package com.sftpftp.jschapi.mapper;

import com.sftpftp.jschapi.entity.BsBroadbandSatisfactionFixEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * BsBroadbandSatisfactionFixDAO继承基类
 */
@Repository
public interface BsBroadbandSatisfactionFixMapper extends MyBatisBaseDao<BsBroadbandSatisfactionFixEntity, String> {
    int insertBatch(@Param("dataList") List<BsBroadbandSatisfactionFixEntity> bsBroadbandSatisfactionFixEntityList);
}