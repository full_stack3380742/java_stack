package com.sftpftp.jschapi;

import cn.hutool.core.date.StopWatch;
import com.azh.common.NamedThreadFactory;
import com.jcraft.jsch.ChannelSftp;
import com.sftpftp.common.SFTPUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 使用Jsch库提供的api连接ssh服务器，不支持ftp服务器连接
 * 支持 sftp 连接
 */
@Slf4j
public class JschDemoApiReadFile {

    private static final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(),
            Runtime.getRuntime().availableProcessors() * 2 - 1, 0, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(1000), new NamedThreadFactory("PullCenterFTTHSATDataService"),
            (r, executor) -> {
                if (!executor.isShutdown()) {
                    try {
                        // 主线程将会被阻塞
                        executor.getQueue().put(r);
                    } catch (InterruptedException e) {
                        log.error("家宽满意度修复功能-线程池添加任务到队列异常: ", e);
                    }
                }
            });

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String host = "localhost";
        String user = "sftp";
        String password = "sftp@sftp";

        try {
            ChannelSftp sftp = SFTPUtils.getChannelSftp(user,password, host, 2222);
            // TODO: Handle your FTP operations here...
            InputStream inputStream = sftp.get("/upload/2024-04-01_外呼目标清单.csv");
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                String str;
                List<Map<String, String>> mapList = new ArrayList<>();
                long pushCount = 100000;
                while (StringUtils.isNotEmpty(str = bufferedReader.readLine()) && pushCount >= 0){
                    Map<String, String> map = buildLineData(str);
                    mapList.add(map);
                    if (mapList.size() == 1000) {
                        CompletableFuture.runAsync(() -> convertDb(mapList));
                        pushCount -= 1000;
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            SFTPUtils.disconnect();
        }
        stopWatch.stop();
        log.info("在线从ftp服务器上读取20w数据话费时间为：" + stopWatch.getTotalTimeMillis() + "ms");
    }

    private static void convertDb(List<Map<String, String>> mapList) {

    }

    /**
     * 根据输入的字符串构建行数据的映射。
     *
     * @param lineDataStr 以管道符('|')分隔的字符串，包含宽带账号、地市和区县信息。
     * @return 返回一个包含宽带账号、地市和区县信息的映射表。
     */
    private static Map<String, String> buildLineData(String lineDataStr) {
        String[] split = lineDataStr.split("\\|");
        Map<String, String> map = new HashMap<>();
        // 分割输入字符串并填充映射表
        // 宽带账号
        map.put("broadbandAccount", split[0]);
        // 地市
        map.put("prefecture", split[1]);
        // 区县
        map.put("district", split[2]);
        return map;
    }
}
