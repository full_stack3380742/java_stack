package com.sftpftp.common;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SFTPUtils {

    private static Session session = null;
    private static ChannelSftp sftp = null;

    public static ChannelSftp getChannelSftp(String userName, String password, String host, int port) {

        try {
            JSch jsch = new JSch();
            session = jsch.getSession(userName, host, port);
            session.setPassword(password);

            session.setConfig("StrictHostKeyChecking", "no");
            session.setTimeout(6000);
            session.connect();

            Channel channel = session.openChannel("sftp");
            channel.connect();
            sftp = (ChannelSftp) channel;
        }catch (Exception e) {
            log.error("连接sftp服务器异常：", e);
        }
        return sftp;
    }

    public static void disconnect() {
        if (session != null) {
            session.disconnect();
        }
        if (sftp != null) {
            sftp.disconnect();
        }
    }
}
