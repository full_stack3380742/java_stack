package com.sftpftp.common;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class JDBCUtils {

    private static String driver = "com.mysql.cj.jdbc.Driver";
    private static Connection connection = null;
    public static Connection getConnection(String url, String user, String password) {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException | SQLException e) {
            log.error("ClassNotFoundException|SQLException: ", e);
        }
        return connection;
    }

    public static void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                log.error("SQLException: ", e);
            }
        }
    }
}
