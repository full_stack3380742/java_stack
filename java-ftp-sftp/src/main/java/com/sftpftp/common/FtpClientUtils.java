package com.sftpftp.common;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import java.io.IOException;

public class FtpClientUtils {
    /**
     * 连接到FTP服务器。
     *
     * @param host FTP服务器的主机名或IP地址。
     * @param port FTP服务器的端口号。
     * @param username 连接FTP服务器的用户名。
     * @param password 连接FTP服务器的密码。
     * @return 成功连接并登录后返回FTPClient对象，如果连接或登录失败则返回null。
     * @throws IOException 如果在连接过程中发生IO异常。
     */
    public static FTPClient connect(String host, int port, String username, String password) throws IOException {
        FTPClient ftpClient = new FTPClient();
        ftpClient.enterLocalPassiveMode();
        ftpClient.setControlEncoding("UTF-8");
        ftpClient.connect(host, port); // 连接到指定的FTP服务器
        int replyCode = ftpClient.getReplyCode(); // 获取服务器响应码
        if (!FTPReply.isPositiveCompletion(replyCode)) { // 检查服务器响应码是否表示连接成功
            ftpClient.disconnect(); // 如果连接失败，则断开连接
            return null;
        }

        boolean loginSuccess = ftpClient.login(username, password); // 使用提供的用户名和密码尝试登录
        if (loginSuccess) { // 如果登录成功
            ftpClient.enterLocalPassiveMode(); // 设置被动模式，以便于后续的数据传输
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE); // 设置文件传输类型为二进制，避免因文件类型不同引起的传输问题
        }
        return ftpClient; // 返回FTPClient对象，供外部调用者使用
    }
}
