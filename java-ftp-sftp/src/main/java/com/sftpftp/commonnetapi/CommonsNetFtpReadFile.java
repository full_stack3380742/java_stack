package com.sftpftp.commonnetapi;

import com.alibaba.fastjson.JSON;
import com.sftpftp.common.FtpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@Slf4j
public class CommonsNetFtpReadFile {
    public static void main(String[] args) {
        FTPClient ftpClient;
        String str;
        try {
            ftpClient = FtpClientUtils.connect("20.26.85.127", 21, "cbossnw", "Sec#2021#");
            FTPFile[] ftpPaths = new FTPFile[0];
            try {
                //log.info("获取ftp服务器连接对象为：{}",  JSON.toJSONString(ftpClient));
                ftpPaths = ftpClient.listFiles("/app/cbossnw/dcpp/ZXAICL/");
            } catch (IOException e) {
                log.error("获取文件目录信息异常：", e);
                return ;
            }
            for (FTPFile ftpFile : ftpPaths) {
                log.error("文件名称为: {}", ftpFile.getName());
            }
            //ftpClient = FtpClientUtils.connect("localhost", 21, "ftp", "ftp@ftp");
            InputStream inputStream = ftpClient.retrieveFileStream("/app/cbossnw/dcpp/ZXAICL/2024-04-15_外呼目标清单.csv");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            List<Map<String, String>> mapList = new ArrayList<>();
            long pushCount = 100000;
            while (StringUtils.isNotEmpty(str = bufferedReader.readLine()) && pushCount >= 0){
                Map<String, String> map = buildLineData(str);
                mapList.add(map);
                if (mapList.size() == 1000) {
                    List<Map<String, String>> insertDataList = new ArrayList<>();
                    insertDataList.addAll(mapList);
                    CompletableFuture.runAsync(() -> convertDb(insertDataList));
                    mapList.clear();
                    pushCount -= 1000;
                }

            }
        } catch (IOException e) {
            log.error("ftp服务器连接异常：", e);
        }
    }

    private static void convertDb(List<Map<String, String>> mapList) {
        log.info("读取数据大小：{}", JSON.toJSONString(mapList.get(0)));
    }


    /**
     * 根据输入的字符串构建行数据的映射。
     *
     * @param lineDataStr 以管道符('|')分隔的字符串，包含宽带账号、地市和区县信息。
     * @return 返回一个包含宽带账号、地市和区县信息的映射表。
     */
    private static Map<String, String> buildLineData(String lineDataStr) {
        String[] split = lineDataStr.split("\\|");
        Map<String, String> map = new HashMap<>();
        // 分割输入字符串并填充映射表
        // 宽带账号
        map.put("broadbandAccount", split[0]);
        // 地市
        map.put("prefecture", split[1]);
        // 区县
        map.put("district", split[2]);
        return map;
    }
}
